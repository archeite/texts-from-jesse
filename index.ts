/**
 * @fileoverview Matrix bot main file.
 * @module bot
 */

import {
	AutojoinRoomsMixin,
	MatrixClient,
	MessageEvent,
	MessageEventContent,
	RustSdkCryptoStorageProvider,
	SimpleFsStorageProvider,
} from "matrix-bot-sdk";
import * as dotenv from "dotenv";
import { checkForTriggers } from "./modules/auto_respond.js";
import { make_fans } from "./modules/make_fans.js";
import { help } from "./modules/help.js";
import { SQLite as Database } from "simplest.db";
import { check_and_update_ratelimit } from "./modules/ratelimit.js";
import joke from "./modules/joke.js";

// Initialize database
const db = new Database({
	path: "./db.sqlite",
});
dotenv.config(); // Load environment variables from .env file

const homeserverUrl = process.env.HOMESERVER ?? ""; // Get homeserver URL from environment variables
const accessToken = process.env.TOKEN ?? ""; // Get access token from environment variables

const storage = new SimpleFsStorageProvider("bot.json"); // Create storage provider for bot.json file
const cryptoProvider = new RustSdkCryptoStorageProvider("./crypto"); // Create crypto storage provider

const client = new MatrixClient(
	homeserverUrl,
	accessToken,
	storage,
	cryptoProvider
); // Create MatrixClient instance
AutojoinRoomsMixin.setupOnClient(client); // Setup auto-join rooms mixin on client

// Listen for room.message event
client.on(
	"room.message",
	async (roomId: string, event: any) => {
		// Don't handle unhelpful events (ones that aren't text messages, are redacted, or sent by us)
		if (
			event.content.msgtype !== "m.text" &&
			event.content.msgtype !== "m.notice"
		)
			return;
		if (event["sender"] === (await client.getUserId())) return;


		const body = event["content"]["body"]; // Get message body

		if (body.startsWith("!j")) {
			if (check_and_update_ratelimit(roomId, true)) {
				// Check and update ratelimit for the room
				await client
					.sendEvent(roomId, "m.reaction", {
						"m.relates_to": {
							rel_type: "m.annotation",
							event_id: event.event_id,
							key: "❌",
						},
					})
					.catch(() => {});
				return false;
			}
			// Check if message starts with "jesse"
			if (body.length > 100) {
				// Send notice if message length is too long
				return await client.sendNotice(
					roomId,
					"I'm not processing that, what do I look like, a fat kid at a Burger King? Make smaller inputs."
				);
			}
			const command = body.split(" ").slice(1); // Split message body by space and remove first element ("jesse") to get command
			switch (command[0]) {
				case "stan":
					// Call make_fans function if command is "stan"
					return make_fans(roomId, event, {
						subject: command.slice(1).join(" "),
					});
				case "help":
					// Call help function if command is "help"
					return help(roomId, event);
				case "joke":
					// Get a new jole
					return joke.onMessage(roomId, event)
				default:
					return await client
						.sendEvent(roomId, "m.reaction", {
							"m.relates_to": {
								rel_type: "m.annotation",
								event_id: event.event_id,
								key: "❔",
							},
						})
						.catch(() => {});
			}
		}

		checkForTriggers(roomId, event); // Call checkForTriggers function for other messages
	}
);

client.start().then(() => console.log("Bot started!")); // Start the bot

export {
	client, // Export MatrixClient instance
	db, // Export database instance
};
