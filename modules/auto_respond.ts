import { MatrixClient, MessageEventContent, MessageEvent } from "matrix-bot-sdk";
import * as fs from "fs";
import { client } from "../index.js";
import { check_and_update_ratelimit } from "./ratelimit.js";

enum TriggerType {
	AUDIO = "audio",
	TEXT = "text",
}

interface Trigger {
	trigger: string;
	responses: string[];
	probability?: number;
	trigger_type?: TriggerType;
}

async function check_responses_file_syntax(): Promise<boolean> {
	try {
		const triggers: Trigger[] = JSON.parse(
			fs.readFileSync("resources/responses.json", "utf-8")
		);
	} catch {
		return false;
	}

	// Perform syntax checking on triggers if needed

	return true;
}

export const checkForTriggers = async (roomId: string, event: any) => {
	if (!await check_responses_file_syntax()) {
		console.error("Responses file is invalid!");
		return;
	}

	const triggers: Trigger[] = JSON.parse(
		fs.readFileSync("resources/responses.json", "utf-8")
	);

	for (const trigger of triggers) {
		if (event.content.body.toLowerCase().includes(trigger.trigger)) {
			const probability = trigger.probability ?? 1;
			if (Math.random() > probability) return;

			if (check_and_update_ratelimit(roomId)) {
				// Check and update ratelimit for the room
				await client
					.sendEvent(roomId, "m.reaction", {
						"m.relates_to": {
							rel_type: "m.annotation",
							event_id: event.event_id,
							key: "❌",
						},
					})
					.catch(() => {});
				return false;
			}

			const chosenResponse =
				trigger.responses[
					Math.floor(Math.random() * trigger.responses.length)
				];

			switch (trigger.trigger_type ?? TriggerType.TEXT) {
				case TriggerType.TEXT:
					await client.sendNotice(roomId, chosenResponse);
					break;
				case TriggerType.AUDIO:
					// Send random audio file, path is at chosenResponse
					const resourcesPath = `resources/${chosenResponse}`;
					const audio = fs.readFileSync(resourcesPath);
					const data = await client.uploadContent(audio);

					await client.sendMessage(roomId, {
						msgtype: "m.audio",
						url: data,
						info: {
							mimetype: "audio/mp4",
						},
						body: "+10 social credit!",
					});
					break;
			}
		}
	}
}