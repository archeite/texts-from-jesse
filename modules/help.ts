import { MatrixClient, MessageEvent, MessageEventContent } from "matrix-bot-sdk";
import { client } from "../index.js";

const helpText = `HELP

Command format: "jesse <command> <parameters>"
COMMANDS

- stan <name>, outputs the "If X has a million fans" copypasta with your name
`;


/**
 * Handles the "help" command, which sends a notice to the given room with a help text containing
 * information about available commands and their format.
 *
 * @param {string} roomId - The ID of the room where the command was triggered.
 * @param {MessageEvent<MessageEventContent>} event - The Matrix message event that triggered the command.
 * @returns {Promise<void>} - A Promise that resolves when the help text has been sent.
 */
export const help = async (roomId: string, event: MessageEvent<MessageEventContent>): Promise<void> => {
	await client.sendNotice(roomId, helpText);
}
