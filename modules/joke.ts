/**
 * Module for handling messages in a chat room.
 * @module messageHandler
 */

import { client } from "../index.js";

/**
 * Handler function for processing messages in a chat room.
 * @async
 * @function onMessage
 * @param {string} roomId - The ID of the chat room.
 * @param {any} event - The event containing the message data.
 * @returns {Promise<string>} - A promise that resolves once the message is processed.
 */
export default {
	onMessage: async (roomId: string, event: any) => {
		// Fetch a joke from a joke API
		const response = await fetch(
			"https://v2.jokeapi.dev/joke/Any?format=json&type=single"
		);

		// Extract the joke from the response and send it as a text message to the chat room
		return client.sendText(roomId, (await response.json()).joke);
	},
};
