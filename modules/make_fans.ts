import { MessageEvent, MessageEventContent } from "matrix-bot-sdk";
import { client } from "../index.js";

/**
 * Handles the "make_fans" command, which sends a notice to the given room with a customized message
 * containing the provided subject.
 *
 * @param {string} roomId - The ID of the room where the command was triggered.
 * @param {MessageEvent<MessageEventContent>} event - The Matrix message event that triggered the command.
 * @param {MakeFansParameters} parameters - Optional parameters for the "make_fans" command.
 * @returns {Promise<void>} - A Promise that resolves when the message has been sent.
 */
export const make_fans = async (roomId: string, event: MessageEvent<MessageEventContent>, parameters?: MakeFansParameters): Promise<void> => {
	if (parameters?.subject) {
		await client.sendNotice(
			roomId,
			`If X has a million fans, then I am one of them. If X has ten fans, then I am one of them. If X has only one fan then that is me. If X has no fans, then that means I am no longer on earth. If the world is against X, then I am against the world.`.replaceAll("X", parameters.subject)
		);
	}
}

/**
 * Interface for the parameters of the "make_fans" command.
 */
interface MakeFansParameters {
	subject: string; // The subject for which to generate the fan message.
}
