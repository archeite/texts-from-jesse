import { DataObj } from "simplest.db/lib/typings/base.js";
import { db } from "../index.js";

/**
 * Room object interface.
 * @interface
 * @property {number|undefined} lastCommandTime - The timestamp of the last command time.
 * @property {number|undefined} lastAutoResponseTime - The timestamp of the last auto-response time.
 */
interface Room {
	lastCommandTime?: number;
	lastAutoResponseTime?: number;
}

/**
 * Function to check and update ratelimit for a given room ID.
 * @function
 * @param {string} roomId - The room ID to check ratelimit for.
 * @param {boolean} command - A boolean flag indicating whether the ratelimit is for a command or an auto-response. Default is false.
 * @returns {boolean} - Returns true if ratelimit is violated, false otherwise.
 */
export const check_and_update_ratelimit = (
	roomId: string,
	command: boolean = false
) => {
	// Create the room key based on the roomId
	const room = `room_${roomId}`;

	// If there is no room entry in the db, create an empty room object
	if (!db.get(room)) db.set(room, {});

	// Get the room object from the db
	let roomObject: Room = db.get(room) as any;

	// Get the current timestamp
	const currentTime = Date.now();

	// Check if the ratelimit is for a command or an auto-response
	if (command) {
		// If there is no lastCommandTime or the time since lastCommandTime is greater than or equal to 5000ms (5 seconds)
		if (
			!roomObject.lastCommandTime ||
			currentTime - roomObject.lastCommandTime >= 3000
		) {
			// Update the lastCommandTime to current time
			roomObject = {
				...roomObject,
				lastCommandTime: currentTime,
			};
			// Save the updated room object in the db
			db.set(room, roomObject as DataObj);
			// Return false indicating no ratelimit violation
			return false;
		}
	} else {
		// If there is no lastAutoResponseTime or the time since lastAutoResponseTime is greater than or equal to 30000ms (30 seconds)
		if (
			!roomObject.lastAutoResponseTime ||
			currentTime - roomObject.lastAutoResponseTime >= 30000
		) {
			// Update the lastAutoResponseTime to current time
			roomObject = {
				...roomObject,
				lastAutoResponseTime: currentTime,
			};
			// Save the updated room object in the db
			db.set(room, roomObject as DataObj);
			// Return false indicating no ratelimit violation
			return false;
		}
	}

	// If the code reaches here, it means the ratelimit is violated, so return true
	return true;
};